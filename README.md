# C++ Game of Life

Terminal based implementation of Game of life

<https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life>

## Requirements

```
Cmake, g++
```

## Usage

Go into a folder of choice and pull the project

```
git pull https://gitlab.com/jhkangas3/game-of-life.git
```

Go into the created folder and create a build folder

```
mkdir build && cd build
```

Make to build the program

```
make
```

Run the program

```
./main
```


## Gameplay and features

First the player is asked to provide board size, with a minimum of 3.

Then a square board is created of the given size, and player can populate the living cells by inputting coordinates in the format X,X. When seeding is done, game can be started with typing "done".

Alternatively, the board can be randomly populated by entering "random". This randomly populates each cell with a 30% chance, and starts the game.

Player can go through the rounds by pressing enter, or exit the game prematurely by typing "exit". The game will go on until a steady state is found and the game exits.

## Author

Juho Kangas
