/*
Game of life
Terminal based implementation
Juho Kangas 2022
*/
#include <iostream>
#include <string>
#include <vector>
#include "functions.h"
#include "draw.h"
#include "game.h"


int main()
{
    int size = 0;
    std::string input;

    int sys = 0;

    while (size < 3){
        std::cout << "Input size of the board\n";
        std::cout << "Minimum size: 3\n";
        std::getline(std::cin, input);
        // Function returns the size and protects from bad input
        size = boardsize(input);
    }
    // Creates a board with size * size dimensions
    std::vector<std::vector<int>> board(size, std::vector<int> (size, 0));

    // Initial seeding loop, exited by inputing "done"
    while (input != "done"){
        // Clear the terminal, check return value to avoid warnings
        sys = system("clear");
        if(sys == -1){
            std::cout << "system clear failed";
        }
        drawBoard(board);
        std::cout << "Input coordinate to set live cell \n";
        std::cout << "Two integers separated by comma (X,X)\n";
        std::cout << "random starts the game with randomly filled board\n";
        std::cout << "done finishes the board setup\n";
        std::getline(std::cin, input);
        if (input == "done"){
            break;
        }
        // Option to fill board randomly with 30% chance of live cell
        if (input == "random"){
            board = createBoard(size);
            break;
        }

        // Returned coordinate change the status of the cells
        std::vector<int> coords = coordinates(input, size);
        if(coords[0] == -1){
            continue;
        } else {
            board[coords[0]][coords[1]] = !board[coords[0]][coords[1]];
        }
        
    }

    // Main gameplay loop
    while(true){

        // Clear the terminal, check return value to avoid warnings
        sys = system("clear");
        if(sys == -1){
            std::cout << "system clear failed";
        }
        // Next iteration of the board state calculated
        board = game(board);

        drawBoard(board);
        std::cout << "Enter to continue, exit to end game\n";
        std::getline(std::cin, input);
        if (input == "exit"){
            break;
        }
        if(board == game(board)){
            std::cout << "Game reached steady state, ending.\n";
            break;
        }
    }

}