#include "draw.h"

// Function to draw the current state of the board
void drawBoard(std::vector<std::vector<int>> board){
    int size = board.size();
        for(int i = 0; i < size; i++)
        {
            for(int j = 0; j < size; j++)
            {
                std::cout << board[j][i] << " ";
            }
            std::cout<< std::endl;
        }
}