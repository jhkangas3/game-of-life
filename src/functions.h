#pragma once
#include <string>
#include <vector>
#include <iostream>
#include <random> 
#include <functional> // bind in random generation

int boardsize(std::string input);

std::vector<std::vector<int>> createBoard(int size);

std::vector<int> coordinates(std::string input, int size);