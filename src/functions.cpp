#include "functions.h"
// Function to change string input to board size
int boardsize(std::string input)
{
    try {
        int size = std::stoi(input);
        if (size >= 3){
            return size;
        }
        else {
            return 0;
        }

    }
    // If stoi fails the conversion, to protect from bad input
    catch (...){
        return 0;
    }
}
// Function returns a board of size, randomly filled content
std::vector<std::vector<int>> createBoard(int size)
{
    std::vector<std::vector<int>> board(size, std::vector<int> (size, 0));

    std::default_random_engine generator;
    std::uniform_int_distribution<int> distribution(1,10);
    auto seeding = std::bind(distribution, generator );

    for(int i = 0; i < size; i++)
        {
            for(int j = 0; j < size; j++)
            {
                if(seeding() < 4){
                    board[j][i] = 1;
                }
            }
        }

    return board;

}

// Function to return valid coordinates from user input
std::vector<int> coordinates(std::string input, int size)
{   
    int temp = 0;
    std::vector<int> coordsOut;
    int comma = input.find(',');
    // No comma found, invalid input
    if(comma == -1){
        coordsOut.push_back(-1);
        return coordsOut;
    }
    // Trying to convert strings to integers within board range
    try {
        temp = std::stoi(input.substr(0, comma));
        if (temp >= 0 && temp < size){
            coordsOut.push_back(temp);
        } else {
            coordsOut.push_back(-1);
            return coordsOut;
        }
        

        temp = std::stoi(input.substr(comma + 1));
        if (temp >= 0 && temp < size){
            coordsOut.push_back(temp);
        } else {
            coordsOut.front() = -1;
            return coordsOut;
        }

        return coordsOut;

    }
    // stoi fails, returns vector with error value -1
    catch (...){
        coordsOut.clear();
        coordsOut.push_back(-1);
        return coordsOut;
    }
    
}