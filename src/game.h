#pragma once
#include <vector>

std::vector<std::vector<int>> game(std::vector<std::vector<int>> board);

int check(int i, int j, int size, std::vector<std::vector<int>> board);