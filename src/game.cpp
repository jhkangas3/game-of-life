#include "game.h"

std::vector<std::vector<int>> game(std::vector<std::vector<int>> board)
{   
    int count = 0;
    int size = board.size();
    // Updated board to be returned after each round
    std::vector<std::vector<int>> newBoard(size, std::vector<int> (size, 0));

    for(int i = 0; i < size; i++)
        {
            for(int j = 0; j < size; j++)
            {   
                // count the live cells around i,j cell
                count = check(i,j,size,board);
                // i,j is alive and has 2 or 3 cells around to maintain living
                if (board[i][j] == 1){
                    if(count == 2 || count == 3){
                        newBoard[i][j] = 1;
                    }
                    
                }
                // i,j is dead and has 3 live surrounding to make it live
                else{
                    if (count == 3){
                        newBoard[i][j] = 1;
                    }
                }
            }

        }

    return newBoard;

}

// Function to check the content surrounding a single cell
int check(int i, int j, int size, std::vector<std::vector<int>> board)
{
    int count = 0;
    // Check top left corner
    if(i - 1 >= 0 && j - 1 >= 0){
        if(board[i - 1][j - 1] == 1){
            count++;
        }
    }
    // Check bottom left corner
    if(i + 1 < size && j - 1 >= 0){
        if(board[i + 1][j - 1] == 1){
            count++;
        }
    }
    // Check top right corner
    if(i - 1 >= 0 && j + 1 < size){
        if(board[i - 1][j + 1] == 1){
            count++;
        }
    }
    // Check bottom right corner
    if(i + 1 < size && j + 1 < size){
        if(board[i + 1][j + 1] == 1){
            count++;
        }
    }
    // Check above
    if(i - 1 >= 0){
        if(board[i - 1][j] == 1){
            count++;
        }
    }    
    // Check left
    if(j - 1 >= 0){
        if(board[i][j - 1] == 1){
            count++;
        }
    } 
    // Check below
    if(i + 1 < size){
        if(board[i + 1][j] == 1){
            count++;
        }
    }
    // Check right
    if(j + 1 < size){
        if(board[i][j + 1] == 1){
            count++;
        }
    }

    return count;

}